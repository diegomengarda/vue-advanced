import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './modules/App.vue'

import DashboardRoutes from './modules/Dashboard/router/routes'
import UsersRoutes from './modules/Users/router/routes'

Vue.use(VueRouter)

const router = new VueRouter();
const routes = Object.assign(DashboardRoutes, UsersRoutes);

router.map(routes);
router.start(App, '#app');